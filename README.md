# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|N|
|Particle Systems|10%|N|
|UI|5%|N|
|Sound effects|5%|N|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : levels
2. Animations : [xxxx]
3. Particle Systems : [xxxx]
4. Sound effects : [xxxx]
5. Leaderboard : [xxxx]

# Bonus Functions Description : 
1. [xxxx] : [xxxx]
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]
