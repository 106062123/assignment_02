
var game = new Phaser.Game(900, 550, Phaser.AUTO, 'game', 
{ preload: preload, create: create, update: update, render: render });


var state; //0 meny  level1 level2 level3

function preload() {

    game.load.image('bullet', 'assets/bullet.png');
    game.load.image('enemyBullet', 'assets/enemy_bullet.png');
    game.load.image('enemyBullet2', 'assets/enemy_bullet2.png');
    game.load.image('enemyBullet3', 'assets/enemy_bullet3.png');
    game.load.image('invader3', 'assets/enemy3.png');
    game.load.spritesheet('invader', 'assets/enemy.png', 32, 32);
    game.load.spritesheet('invader2', 'assets/enemy2.png', 32, 32)
    game.load.image('ship', 'assets/player.png');
    game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
    game.load.image('sky', 'assets/background.png');
    game.load.image('menu_back','assets/background2.jpg');
    state = 0;

}


var player;
var enemy;
var bullets;
var bulletTime = 0;
var cursors;
var fireButton;
var explosions;
var sky;
var menu_back;
var score = 0;
var scoreString = '';
var scoreText;
var lives;
var bosslives;
var enemyBullet;
var firingTimer = 0;
var stateText;
var livingEnemies = [];

function create() {
    game.physics.startSystem(Phaser.Physics.ARCADE);

    //  The scrolling sky background
    if(state == 0){
        sky = game.add.tileSprite(0, 0, 900, 550, 'menu_back');

        game.add.text(210, 30, "Raiden Potter", { font: '72px Arial', fill: '#fff' });
        game.add.text(280, 400, "Press any key to START", { font: '24px Arial', fill: '#fff' });
    }
    else if(state == 1 || state == 2 || state == 3){
        sky = game.add.tileSprite(0, 0, 900, 550, 'sky');

        //  Our bullet group
        bullets = game.add.group();
        bullets.enableBody = true;
        bullets.physicsBodyType = Phaser.Physics.ARCADE;
        bullets.createMultiple(30, 'bullet');
        bullets.setAll('anchor.x', 0.5);
        bullets.setAll('anchor.y', 1);
        bullets.setAll('outOfBoundsKill', true);
        bullets.setAll('checkWorldBounds', true);

        // The enemy's bullets
        enemyBullets = game.add.group();
        enemyBullets.enableBody = true;
        enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        if(state == 1){
            enemyBullets.createMultiple(30, 'enemyBullet');
        }
        else if(state == 2){
            enemyBullets.createMultiple(30, 'enemyBullet2');
        }
        else if(state == 3){
            enemyBullets.createMultiple(30, 'enemyBullet3');
        }
        enemyBullets.setAll('anchor.x', 0.5);
        enemyBullets.setAll('anchor.y', 1);
        enemyBullets.setAll('outOfBoundsKill', true);
        enemyBullets.setAll('checkWorldBounds', true);

        //  player
        player = game.add.sprite(400, 500, 'ship');
        player.anchor.setTo(0.5, 0.5);
        game.physics.enable(player, Phaser.Physics.ARCADE);

        //  enemy
        enemy = game.add.group();
        enemy.enableBody = true;
        enemy.physicsBodyType = Phaser.Physics.ARCADE;

        createEnemy();

        //  The score
        scoreString = 'Score : ';
        if(state == 1){
            score = 0;
        }
        scoreText = game.add.text(10, 10, scoreString + score, { font: '34px Arial', fill: '#fff' });

        //  Lives
        lives = game.add.group();
        game.add.text(680, 10, 'Lives : ', { font: '34px Arial', fill: '#fff' });

        bosslives = game.add.group();

        //  Text
        stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#fff' });
        stateText.anchor.setTo(0.5, 0.5);
        stateText.visible = false;

        for (var i = 0; i < 3; i++) 
        {
            var ship = lives.create(860 - (30 * i), 30, 'ship');
            ship.anchor.setTo(0.5, 0.5);
            ship.angle = 0
            ship.alpha = 0.4;
        }
        for(var j =0;j<10;j++){
            bosslives.create();
        }

        //  An explosion pool
        explosions = game.add.group();
        explosions.createMultiple(30, 'kaboom');
        explosions.forEach(setupInvader, this);

        //  And some controls to play the game with
        cursors = game.input.keyboard.createCursorKeys();
        fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    }
}

function createEnemy () {

    if(state == 1){
      for (var y = 0; y < 3; y++)
        {
            for (var x = 0; x < 10; x++)
            {
                var alien = enemy.create(x * 48, y * 50, 'invader');
                alien.anchor.setTo(0.5, 0.5);
                alien.animations.add('fly', [ 0, 1, 2, 3 ], 5, true);
                alien.play('fly');
                alien.body.moves = false;
            }
        }

        enemy.x = 100;
        enemy.y = 50;  
    }
    else if(state == 2){
        for (var y = 0; y < 5; y++)
        {
            for (var x = 0; x < 10; x++)
            {
                var alien = enemy.create(x * 40, y * 50, 'invader2');
                alien.anchor.setTo(0.5, 0.5);
                alien.animations.add('fly', [ 0, 1, 2, 3 ], 5, true);
                alien.play('fly');
                alien.body.moves = false;
            }
        }

        enemy.x = 50;
        enemy.y = 50;  
    }
    else if(state==3){
        var alien = enemy.create(500, 50, 'invader3');
        alien.anchor.setTo(0.5, 0.5);
        enemy.x = 200;
        enemy.y = 250;
    }

    //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
    var tween = game.add.tween(enemy).to( { x: 200 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);

    //  When the tween loops it calls descend
    tween.onLoop.add(descend, this);
}

function setupInvader (invader) {

    invader.anchor.x = 0.5;
    invader.anchor.y = 0.5;
    invader.animations.add('kaboom');

}

function descend() {

    enemy.y += 10;

}


function update() {
    game.input.keyboard.onPressCallback = function(){
        if(state == 0){
            state = 1;
            console.log("state change",state);
            create();
        }
    };
    
    if(state == 1 || state == 2 || state == 3){
        //  Scroll the background
        sky.tilePosition.x += 2;

        if (player.alive)
        {
            //  Reset the player, then check for movement keys
            player.body.velocity.setTo(0, 0);

            if (cursors.left.isDown)
            {
                player.body.velocity.x = -200;
                if(player.scale.x>0){
                    player.scale.x *= -1;
                }
            }
            else if (cursors.right.isDown)
            {
                player.body.velocity.x = 200;
                if(player.scale.x<0){
                    player.scale.x *= -1;
                }
            }
            else if (cursors.up.isDown)
            {
                player.body.velocity.y = -200;
            }
            else if (cursors.down.isDown)
            {
                player.body.velocity.y = 200;
            }

            //  Firing?
            if (fireButton.isDown)
            {
                fireBullet();
            }

            if (game.time.now > firingTimer)
            {
                enemyFires();
            }

            //  Run collision
            game.physics.arcade.overlap(bullets, enemy, collisionHandler, null, this);
            game.physics.arcade.overlap(enemyBullets, player, enemyHitsPlayer, null, this);
        }
    }

    else if(state == 3){

    }
}

function render() {

    // for (var i = 0; i < enemy.length; i++)
    // {
    //     game.debug.body(enemy.children[i]);
    // }

}

function collisionHandler (bullet, alien) {

    //  When a bullet hits an alien we kill them both
    bullet.kill();
    if(state == 1||state == 2){
        alien.kill();
    }
    else if(state == 3){
        live = bosslives.getFirstAlive();

        if (live)
        {
            live.kill();
            
            if (bosslives.countLiving() < 1)
            {
                enemy.removeAll();
                enemyBullets.callAll('kill');
            }
        }
    }
  
    //  Increase the score
    score += 10;
    scoreText.text = scoreString + score;

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(alien.body.x, alien.body.y);
    explosion.play('kaboom', 30, false, true);

    if (enemy.countLiving() == 0)
    {
        state += 1;
        console.log("state",state);
        // WIN!!!!!!!!!!!!!1
        if(state == 4){       
            enemyBullets.callAll('kill',this);
            stateText.text = " You Won, \n Click to restart";
            stateText.visible = true;

            //the "click to restart" handler
            game.input.onTap.addOnce(restart,this);
        }
        else{
            // next level
            create();
        }
    }

}

function enemyHitsPlayer (player,bullet) {
    
    bullet.kill();

    live = lives.getFirstAlive();

    if (live)
    {
        live.kill();

    }

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(player.body.x, player.body.y);
    explosion.play('kaboom', 30, false, true);

    // When the player dies
    if (lives.countLiving() < 1)
    {
        player.kill();
        enemyBullets.callAll('kill');

        stateText.text=" GAME OVER \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }

}

function enemyFires () {

    //  Grab the first bullet we can from the pool
    enemyBullet = enemyBullets.getFirstExists(false);

    livingEnemies.length=0;

    enemy.forEachAlive(function(alien){

        // put every living enemy in an array
        livingEnemies.push(alien);
    });


    if (enemyBullet && livingEnemies.length > 0)
    {
        
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);

        // randomly select one of them
        var shooter=livingEnemies[random];
        // And fire the bullet from this enemy
        enemyBullet.reset(shooter.body.x, shooter.body.y);

        game.physics.arcade.moveToObject(enemyBullet,player,120);
        if(state == 1){
            firingTimer = game.time.now + 2000;
        }
        else if(state == 2){
            firingTimer = game.time.now + 1000;
        }
        else if(state == 3){
            firingTimer = game.time.now + 100;
        }
    }

}

function fireBullet () {

    //  To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bulletTime)
    {
        //  Grab the first bullet we can from the pool
        bullet = bullets.getFirstExists(false);

        if (bullet)
        {
            if(state == 1 || state==2){
                //  And fire it
                bullet.reset(player.x, player.y + 8);
                bullet.body.velocity.y = -400;
                bulletTime = game.time.now + 200;
            }
            else if(state == 3){
                bullet.reset(player.x+8, player.y);
                bullet.body.velocity.x = +400;
                bulletTime = game.time.now + 200;
            }
        }
    }

}

function resetBullet (bullet) {

    //  Called if the bullet goes out of the screen
    bullet.kill();

}
function restart () {

    //  A new level starts
    state = 1;
    

    //resets the life count
    lives.callAll('revive');
    //  And brings the enemy back from the dead :)
    enemy.removeAll();

    //revives the player
    player.revive();
    //hides the text
    stateText.visible = false;
    create();
}
